const express = require('express');
const app = express();

const nicehash = require('./lib/nicehash');
const converter = require('./lib/converter');

app.get('/', async (request, response, next) => {
    try {
        const orders = await nicehash.getOrders(nicehash.algorithm.DAGGERHASHIMOTO);
        const conversion = await converter.convert(converter.crypto.BITCOIN);
        console.log(orders)
        const modifiedorders = orders.map(order => ({
            workers: order.workers,
            price: order.price * conversion.price_eur * parseInt(order.workers)
        }))
        response.send(modifiedorders);
    } catch (err) {
        next(err);
    }
    
    //price_eur
});

app.use((err, request, response, next) => {
    return response.status(500).send(err.message);
});

app.listen(process.env.PORT || 3000, () => console.log('server started'));
