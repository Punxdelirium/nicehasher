const fetch = require('node-fetch');
const URI = 'https://api.coinmarketcap.com/v1/ticker/';

const crypto = {
    ETHEREUM: 'ethereum',
    BITCOIN: 'bitcoin'
};

const currency = {
    EUR: 'EUR'
}

const convert = async (cr = crypto.ETHEREUM, curr = currency.EUR) => fetch(`${URI}${cr}/?convert=${curr}`).then(result => result.json()).then(json => json[0]);

module.exports.convert = convert;
module.exports.crypto = crypto;
module.exports.currency = currency;