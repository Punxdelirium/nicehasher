const fetch = require('node-fetch');
const URI = 'https://api.nicehash.com/api';

const algorithm = {
    DAGGERHASHIMOTO: 20
}

const getOrders = async (algorythm, filter) => fetch(`${URI}?method=orders.get&location=0&algo=${algorythm}`).then(result => result.json()).then(json => json.result.orders);

module.exports.getOrders = getOrders;
module.exports.algorithm = algorithm;